public class Disciplina{

//Atributos
	private String nome;
	private String codigoDisciplina;
	private String professor;

//Metodos
	public void setNome(String umNome){
		this.nome = umNome;
	}
	public String getNome(){
		return nome;
	}
	public void setCodigoDisciplina(String umCodigoDisciplina){
                this.codigoDisciplina = umCodigoDisciplina;
        }
        public String getCodigoDisciplina(){
                return codigoDisciplina;
        }
	public void setProfessor(String umProfessor){
                this.professor = umProfessor;
        }
        public String getProfessor(){
                return professor;
        }


}
